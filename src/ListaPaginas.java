public class ListaPaginas {
    // Estos son los nodos, les puse pagina porque es más significativo
    class Pagina {
        int num; // Numero de la pagina
        int nVeces; // Cuantas veces sale en esa pagina
        Pagina siguiente;

        public Pagina(int p) {
            num = p;
            nVeces = 1;
            siguiente = null;
        }
    }

    Pagina head = null;
    public ListaPaginas(int paginaInicial) {
        head = new Pagina(paginaInicial);
    }

    void insertar(int p) {
        Pagina prev = head;
        for (Pagina actual=head; actual!=null; actual=actual.siguiente) {
            if (actual.num == p) {
                actual.nVeces++;
                return;
            }
            prev = actual;
        }
        prev.siguiente = new Pagina(p);
    }
}
