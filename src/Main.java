import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner tcld = new Scanner(System.in);
        System.out.print("Ingrese el nombre del archivo a leer: ");
        String archivoNombre = tcld.next();
        String textoArchivo = "";
        tcld.close();
        Index indiceAlfabetico = new Index();
        int numeroPagina = 1;
        try {
            Scanner lectorArchivo = new Scanner(new File(archivoNombre));
            while (lectorArchivo.hasNext()) {
                textoArchivo += lectorArchivo.nextLine() + "\n";
                // Para contar la pagina actual
                if(textoArchivo.contains("|")) {
                    numeroPagina++;
                }
                // Supongo que es doble backslash para el backslash normal porque sino me lo toma mal
                if(textoArchivo.contains("\\")) {
                    textoArchivo = textoArchivo.substring(0, textoArchivo.length()-1);
                    indiceAlfabetico.insert(textoArchivo, numeroPagina);
                }
            }
        } catch (Exception e) {
            System.out.println("arhivo no valido");
        }
        indiceAlfabetico.printIndice();
    }
}